#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import template
from ..speedcams import get_available_attributes

register = template.Library()  # django thing, required


@register.simple_tag
def new_record_form(attributes=get_available_attributes()):
    html = '<div hidden id=new_record_form>'
    html += _create_header()
    html += _create_body(attributes)
    html += '</div>'
    return html


def _create_header():
    html = '<div id=form_header>'
    html += 'Dodaj nowy rekord'
    html += _create_X_button()
    html += '</div>'
    return html


def _create_X_button():
    html = '<div id=form_header_X'
    html += ' onclick="close_new_record_form()">✗'
    html += '</div>'
    return html


def _create_body(attributes):
    html = _create_table_with_inputs(attributes)
    html += _create_cancel_and_confirm_buttons()
    return html


def _create_table_with_inputs(attributes):
    html = '<table style="margin:1em">'
    for attribute in attributes:
        polski_attr = _polski_attribute(attribute)
        html += '<tr>'
        html += '<td>%s:</td>' % polski_attr
        html += '<td>'
        html += '<input size=22 class=new_record_input id=form_%s>' % attribute
        html += '</td>'
        html += '</tr>'
    html += '</table>'
    return html


def _polski_attribute(attribute):
    if attribute == 'street':
        return 'ulica'
    elif attribute == 'hours':
        return 'godziny'
    elif attribute == 'radius':
        return 'promień'
    elif attribute == 'day':
        return 'dzień'
    elif attribute == 'month':
        return 'miesiąc'
    elif attribute == 'year':
        return 'rok'
    elif attribute == 'latitude':
        return 'szerokość'
    elif attribute == 'longitude':
        return 'długość'


def _create_cancel_and_confirm_buttons():
    html = '<div style="margin: 0.5em">'
    html += '<button style="width:49%"'
    html += ' onclick="close_new_record_form()">Anuluj</button>'

    html += '<button style="width:49%"'
    html += ' onclick="confirm_new_record_form()">Gotowe</button>'
    html += '</div>'
    return html
