"""Helper module, containing all custom logger cases."""

import logging as _logging
_logger = _logging.getLogger('registration')


def account_activation(username):
    """User activated their account."""
    _logger.info('user "%s" activated their account' % username)


def account_registration(username):
    """User registered their account."""
    _logger.info('user "%s" registered their account' % username)


def password_change(username):
    """User changed their password."""
    _logger.info('user "%s" changed their password' % username)


def password_reset(username):
    """User reset their password."""
    _logger.info('user "%s" reset their password' % username)


def download_success(username, ip_address):
    """User authenticated and downloaded speedcams data."""
    if username:
        _logger.info('user "%s" ip <%s> downloaded speedcams data' % (username, ip_address))
    else:
        _logger.info('unknown user ip <%s> downloaded speedcams data' % ip_address)


def unknown_user_ip_block(ip_address):
    """Somebody failed to authenticate too many times without giving a username."""
    _logger.info(
        'unknown user failed to authenticate due to ip <%s> block' % ip_address)


def ip_block(username):
    """User failed to authenticate too many times."""
    _logger.info('user "%s" failed to authenticate due to ip block' % username)


def unknown_user_download_fail(ip_address):
    """Somebody didn't support username and failed to authenticate."""
    _logger.info('failed to download speedcams data for ip <%s>' % ip_address)


def download_fail(username):
    """User failed to authenticate."""
    _logger.info('user "%s" failed to download speedcams data' % username)


def mobile_register_success(username):
    """User has registered their account."""
    _logger.info('user "%s" registered their account' % username)


def mobile_register_fail(ip_address):
    """Somebody failed to create their account."""
    _logger.info('registration fail for ip <%s>' % ip_address)


def mobile_logout(username, download_counter):
    """User has closed the mobile application
    after downloading speedcams number of times."""
    string = ''
    if username:
        string += 'user "%s" closed app' % username
    else:
        string += 'unknown user closed app'
    if download_counter:
        string += ', download count %s' % download_counter
    else:
        string += ', unknown download count'
    _logger.info(string)
