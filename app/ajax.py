# -*- coding: utf-8 -*-
from django.http import JsonResponse
from django.contrib.admin.views.decorators import staff_member_required
import speedcams
import json


@staff_member_required
def speedcams_interface(request):
    if request.POST.get('get') == 'all':
        return _speedcams_data(request)
    elif request.POST.get('create'):
        return _create_and_confirm(request)
    elif request.POST.get('edit'):
        return _edit_and_confirm(request)
    elif request.POST.get('delete'):
        return _delete_and_confirm(request)
    else:
        return _wrong_input_error()


def _speedcams_data(request):
    data = speedcams.get_all()
    if isinstance(data, list):
        return JsonResponse({'success': True, 'data': data})
    return JsonResponse({'success': False})


def _delete_and_confirm(request):
    speedcam_id = request.POST.get('delete')
    try:
        speedcams.delete_from_database(speedcam_id)
    except Exception, error:
        return JsonResponse({'success': False, 'error': error})
    return JsonResponse({'success': True})


def _create_and_confirm(request):
    speedcam = request.POST.get('create')

    try:
        speedcam = json.loads(speedcam)
    except Exception, error:
        return JsonResponse({'success': False, 'error': str(error)})

    result = speedcams.create(speedcam)

    if result['success']:
        return JsonResponse({'success': True, 'id': result['id']})
    elif 'error' in result:
        return JsonResponse({'success': False, 'error': result['error']})
    elif 'matches' in result:
        return JsonResponse({'success': False, 'matches': result['matches']})


def _edit_and_confirm(request):
    speedcam_id = request.POST.get('edit')
    attribute = request.POST.get('attribute')
    new_value = request.POST.get('new_value')

    try:
        speedcams.edit(speedcam_id, attribute, new_value)
    except Exception, error:
        return JsonResponse({'success': False, 'error': str(error)})
    return JsonResponse({'success': True})


def _wrong_input_error():
    return JsonResponse({'success': False, 'error': 'wrong input'})
