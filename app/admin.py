from django.contrib import admin
import models

admin.site.register(models.Speedcam)


class Auth_fail_admin(admin.ModelAdmin):
    readonly_fields = ('timestamp',)

admin.site.register(models.Failed_authentication, Auth_fail_admin)
