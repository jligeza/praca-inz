from django.db import models
import time


class Speedcam(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    radius = models.IntegerField()
    street = models.CharField(max_length=100)
    hours = models.CharField(max_length=50)
    day = models.IntegerField()
    month = models.IntegerField()
    year = models.IntegerField()


class Failed_authentication(models.Model):
    ip = models.CharField(max_length=20)
    username = models.CharField(max_length=100,default="unknown")
    timestamp = models.DateTimeField(auto_now_add=True)
    auth_attempts = models.IntegerField(default=1)
    is_blocked = models.BooleanField(default=False)

    def increment_fail_counter(self):
        self.auth_attempts += 1
        if self.auth_attempts > 4:
            self.is_blocked = True

    def try_removing_blockade(self):
        now = int(time.time())
        timestamp_seconds = int(time.mktime(self.timestamp.timetuple()))
        thirty_minutes = 60 * 30
        if now - timestamp_seconds >= thirty_minutes:
            self.delete()
            return True
