from registration.signals import user_activated, user_registered
import my_logger


def _on_account_activation(sender, **kwargs):
    my_logger.account_activation(kwargs['user'].username)


def _on_account_registration(sender, **kwargs):
    my_logger.account_registration(kwargs['user'].username)


def connect_signals():
    user_activated.connect(_on_account_activation)
    user_registered.connect(_on_account_registration)
