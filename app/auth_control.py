#!/usr/bin/env python
# -*- coding: utf-8 -*-
import app.models as models


def get_client_address(request):
    """Returns ip address for given request."""
    try:
        return request.META['HTTP_X_FORWARDED_FOR']
    except:
        return request.META['REMOTE_ADDR']


def ip_address_blocked(client_address):
    try:
        lock = models.Failed_authentication.objects.get(ip=client_address)
        return _lock_status(lock)
    except:
        return False


def _lock_status(lock):
    if lock.is_blocked:
        if lock.try_removing_blockade() is True:
            return False
        else:
            return True
    else:
        return False


def create_or_increment_auth_lock(client_address, username):
    try:
        lock = models.Failed_authentication.objects.get(ip=client_address)
        lock.increment_fail_counter()
        lock.save()
    except:
        models.Failed_authentication.objects.create(
            ip=client_address, username=username)
