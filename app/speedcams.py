# -*- coding: utf-8 -*-
import app.models as models
import re

_available_attributes = ('street', 'hours', 'day', 'month',
                         'year', 'radius', 'latitude', 'longitude',)


def get_available_attributes():
    return _available_attributes


def get_all():
    """
    Fetch all speedcams from database.
    *****
    Returns:
        list with speedcams
    """
    pack = []
    speedcams = None
    try:
        speedcams = models.Speedcam.objects.all()
    except:
        return []
    for item in speedcams:
        pack.append({'id': item.id,
                     'latitude': item.latitude,
                     'longitude': item.longitude,
                     'radius': item.radius,
                     'street': item.street,
                     'hours': item.hours,
                     'month': item.month,
                     'day': item.day,
                     'year': item.year})
    return pack


def delete_from_database(speedcam_id):
    """
    Delete record from database.
    *****
    Arguments:
        id
    Raises:
        Exception - error message
    """
    try:
        speedcam = models.Speedcam.objects.get(id=speedcam_id)
        speedcam.delete()
        return True
    except Exception, error:
        return str(error)


def create(speedcam):
    """
    Create new record and save it in database.
    *****
    Arguments:
        speedcam dict in json format
    Returns:
        Success: {'success': True, 'id': item_id}
        Failure: {'success': False, 'matches': matched attributes in dict}
        Failure: {'success': False, 'error': error message}
    """
    result = Regex_validation.check_all_values(speedcam)
    if result['success'] is False:
        if 'matches' in result:
            return {'success': False, 'matches': result['matches']}
        elif 'error' in result:
            return {'success': False, 'error': result['error']}

    try:
        new_id = _save_in_database(speedcam)
        return {'success': True, 'id': new_id}
    except Exception, error:
        return {'success': False, 'error': error}


def _save_in_database(speedcam):
    """
    Attribute:
        speedcam (dict)
    Raises:
        Exception - error message
        TypeError - given wrong object
    """
    if isinstance(speedcam, dict):
        try:
            speedcam = models.Speedcam.objects.create(
                street=speedcam['street'],
                latitude=speedcam['latitude'],
                longitude=speedcam['longitude'],
                radius=speedcam['radius'],
                hours=speedcam['hours'],
                month=-1 if speedcam['month'] == '*' else speedcam['month'],
                day=-1 if speedcam['day'] == '*' else speedcam['day'],
                year=-1 if speedcam['year'] == '*' else speedcam['year'])
            return speedcam.id
        except Exception, error:
            raise Exception('record creation fail: ' + str(error))
    else:
        raise TypeError('error: expected a dict, given ' + str(type(speedcam)))


class Regex_validation():
    # TODO sprawdzać, czy godziny się zgadzają
    street = ur'^.{1,50}$'
    # hours example: 00:00-23:59
    hours = r'^([0-1][0-9]|[0-2][0-3]):[0-5][0-9]-' +\
        '([0-1][0-9]|[0-2][0-4]):[0-5][0-9]$'
    day = r'^(\*|[1-9]|[12][0-9]|3[01])$'
    month = r'^(\*|[1-9]|1[0-2])$'
    year = r'^(\*|20[0-9]{2})$'
    radius = r'^[1-9][0-9]{1,3}$'
    latitude = r'^[0-9]+(\.[0-9]*)*$'
    longitude = r'^[0-9]+(\.[0-9]*)*$'

    @classmethod
    def check_all_values(cls, input_dict):
        """
        Check integrity of given speedcam dict.
        *****
        Returns:
            Success: {'success': True}
            Failure: {'success': False, 'matches': matched attributes in dict}
            Failure: {'success': False, 'error': error message}
        """
        individual_checks = {}
        to_check = ('street', 'hours', 'day', 'month', 'year',
                    'radius', 'latitude', 'longitude',)
        for attribute in to_check:
            try:
                try:
                    cls.check_value(attribute, input_dict[attribute])
                    individual_checks[attribute] = True
                except AttributeError:
                    individual_checks[attribute] = False
            except KeyError, error:
                return {'success': False, 'error': error}

        if cls._check_if_all_values_passed(individual_checks) is False:
            return {'success': False, 'matches': individual_checks}

        return {'success': True}

    @classmethod
    def _check_if_all_values_passed(cls, attributes):
        for key in attributes.keys():
            if attributes[key] is False:
                return False
        return True

    @classmethod
    def check_value(cls, what, item):
        """
        Check integrity of given attribute.
        *****
        Raises:
            AttributeError - failed to validate given key
            KeyError - no such key
        """
        if what == 'street':
            if not re.match(cls.street, unicode(item)):
                raise AttributeError
        elif what == 'hours':
            if not re.match(cls.hours, unicode(item)):
                raise AttributeError
            cls._check_hours_order(item)
        elif what == 'day':
            if not re.match(cls.day, unicode(item)):
                raise AttributeError
        elif what == 'month':
            if not re.match(cls.month, unicode(item)):
                raise AttributeError
        elif what == 'year':
            if not re.match(cls.year, unicode(item)):
                raise AttributeError
        elif what == 'radius':
            if not re.match(cls.radius, unicode(item)):
                raise AttributeError
        elif what == 'latitude':
            if not re.match(cls.latitude, unicode(item)):
                raise AttributeError
        elif what == 'longitude':
            if not re.match(cls.longitude, unicode(item)):
                raise AttributeError
        else:
            raise KeyError('no such key: ' + what)

    @staticmethod
    def _check_hours_order(hours):
        first_hour = int(hours[0:2])
        second_hour = int(hours[6:8])
        first_minutes = int(hours[3:5])
        second_minutes = int(hours[9:11])

        if first_hour > second_hour:
            raise AttributeError
        if first_hour == second_hour:
            if first_minutes >= second_minutes:
                raise AttributeError

    def get_patterns(cls):
        """Return all regex patterns in a dict."""
        return {'street': cls.street, 'hours': cls.hours, 'day': cls.day,
                'month': cls.month, 'year': cls.year, 'radius': cls.radius,
                'latitude': cls.latitude, 'longitude': cls.longitude,
                }


def validate_single_attribute(key, value):
    """
    Validate a single speedcam attribute.
    *****
    Arguments:
        key, value
    Raises:
        AttributeError - expected a single key
        KeyError - error message
    """
    try:
        Regex_validation.check_value(key, value)
    except AttributeError:
        raise AttributeError('regex check failed')
    except KeyError:
        raise KeyError('no such key')


def edit(speedcam_id, attribute, new_value):
    """
    Edit record.
    *****
    Arguments:
        speedcam_id, attribute, new_value
    Raises:
        Exception - error message
    """
    try:
        validate_single_attribute(attribute, new_value)
    except Exception, error:
        raise Exception(str(error))

    speedcam = None
    try:
        speedcam = models.Speedcam.objects.get(id=speedcam_id)
    except:
        speedcam = models.Speedcam.objects.create()
    try:
        _change_attribute_of_speedcam(speedcam, attribute, new_value)
    except Exception, error:
        raise Exception('record edit error: ' + str(error))


def _change_attribute_of_speedcam(speedcam, attribute, new_value):
    if attribute in ('day', 'month', 'year'):
        _change_date(speedcam, attribute, new_value)
    else:
        _change_any_attribute_but_date(speedcam, attribute, new_value)


def _change_date(speedcam, attribute, new_value):
    """Star stands for any date, but database only accepts
    integers, so -1 must be set."""
    if new_value == '*':
        setattr(speedcam, attribute, -1)
    else:
        setattr(speedcam, attribute, new_value)
    speedcam.save()


def _change_any_attribute_but_date(speedcam, attribute, new_value):
    setattr(speedcam, attribute, new_value)
    speedcam.save()
