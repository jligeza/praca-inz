# -*- coding: utf-8 -*-
import app.models as models
from django.shortcuts import render
from django.contrib.auth import authenticate as django_auth
from django.contrib.auth.forms import PasswordResetForm
from django.http import JsonResponse
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt

import auth_control
import messages
import my_logger

from registration.forms import RegistrationForm


def password_change_wrapper(request):
    """Wraps password change event to log it."""
    my_logger.password_change(request.user.username)
    return render(request, 'registration/password_change_done.html')


def password_reset_wrapper(request):
    """Wraps password reset event to log it."""
    if request.user.username != '':
        my_logger.password_reset(request.user.username)
    return render(request, 'registration/password_reset_done.html')


def home(request):
    """Displays main site."""
    return render(request, 'home.html', {'user': request.user})


@staff_member_required
def admin_interface(request):
    """Displays admin interface."""
    return render(request, 'admin_interface.html', {'user': request.user})


# CSRF not needed, because mobile_hello doesn't use session.
@csrf_exempt
def mobile_hello(request):
    """Notifies server when user logs in."""
    ip_address = auth_control.get_client_address(request)
    username = request.POST.get('username')
    my_logger.download_success(username, ip_address)
    return JsonResponse({'success': True})


# CSRF not needed, because mobile_bye doesn't use session.
@csrf_exempt
def mobile_logout(request):
    """Notifies server when user quits the application."""
    username = request.POST.get('username')
    download_counter = request.POST.get('download_counter')
    my_logger.mobile_logout(username, download_counter)
    return JsonResponse({'success': True})


# CSRF not needed, because mobile_password_reset doesn't use session.
@csrf_exempt
def mobile_password_reset(request):
    """Sends mail with password reset link."""
    my_form = PasswordResetForm(request.POST)
    if my_form.is_valid():
        return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False})


# CSRF not needed, because mobile_login doesn't use session.
@csrf_exempt
def mobile_login(request):
    """
    Simple login, giving JSON speedcams on success.

    Requires username and password to be passed by POST method.
    Returns speedcams or error message.

    Returns:
        On authentication success:
            {'success': True, 'data': speedcams}
        On authentication failure:
            {'success': False, 'error': error_message}
    """
    client_address = auth_control.get_client_address(request)
    if auth_control.ip_address_blocked(client_address):
        return _send_error(request, messages.IP_BLOCK)

    login_attempt = _authenticate(request)
    if login_attempt == messages.LOGIN_SUCCESS:
        return _send_speedcams(request)
    else:
        username = _get_username(request)
        auth_control.create_or_increment_auth_lock(
            client_address, username)
        return _send_error(request, login_attempt)


def _get_username(request):
    username = request.POST.get('username')
    if not username:
        username = 'unknown'
    return username


def _authenticate(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = django_auth(username=username, password=password)
    return _check_authentication_status(user)


def _check_authentication_status(user):
    if user is not None:
        if user.is_active:
            return messages.LOGIN_SUCCESS
        else:
            return messages.INACTIVE_ACCOUNT
    else:
        return messages.LOGIN_FAIL


def _send_speedcams(request):
    """Send speedcams in json format."""
    speedcams = _get_all_speedcams()
    return JsonResponse({'success': True, 'data': speedcams})


def _get_all_speedcams():
    pack = []
    speedcams = models.Speedcam.objects.all()
    for item in speedcams:
        pack.append({'id': item.id,
                     'latitude': item.latitude,
                     'longitude': item.longitude,
                     'radius': item.radius,
                     'street': item.street,
                     'hours': item.hours,
                     'month': item.month,
                     'day': item.day,
                     'year': item.year})
    return pack


def _send_error(request, error_message):
    """Send error message, and log the event."""
    if error_message == messages.IP_BLOCK:
        _log_ip_block(request)
    elif error_message in (messages.INACTIVE_ACCOUNT, messages.LOGIN_FAIL):
        _log_login_fail(request)
    return JsonResponse({'success': False, 'error': error_message})


def _log_ip_block(request):
    username = request.POST.get('username')
    if username is None:
        ip_address = auth_control.get_client_address(request)
        my_logger.unknown_user_ip_block(ip_address)
    else:
        my_logger.ip_block(username)


def _log_login_fail(request):
    username = request.POST.get('username')
    if username is None:
        ip_address = auth_control.get_client_address(request)
        my_logger.unknown_user_download_fail(ip_address)
    else:
        my_logger.download_fail(username)


# CSRF not needed, because mobile_register doesn't use session.
@csrf_exempt
def mobile_register(request):
    """
    Simple registration, automatically send speedcams on success.

    Requires username, password, repeat password and e-mail
    to be passed by POST method. If registration was successful,
    send speedcams in JSON format. Validates input data, like
    password strength, or e-mail address format.

    Returns:
        On registration success:
            {'success': True, 'data': speedcams}
        On registration failure:
            {'success': False, 'errors': validation_error_list}
    """
    form = RegistrationForm(request.POST)
    try:
        return _register_account(form)
    except ValueError:
        return _registration_fail(request, form)


def _register_account(form):
    """Register account, send speedcams and log it."""
    form.save()
    speedcams = _get_all_speedcams()
    username = form['username'].value()
    my_logger.mobile_register_success(username)
    return JsonResponse({'success': True, 'data': speedcams})


def _registration_fail(request, form):
    """Send validation errors, and log it with client's ip address."""
    ip_address = auth_control.get_client_address(request)
    my_logger.mobile_register_fail(ip_address)
    errors = [e for e in form.errors]
    return JsonResponse({'success': False, 'errors': errors})
