"""Helper module, containing messages sent to client."""

LOGIN_SUCCESS = 'login success'
LOGIN_FAIL = 'invalid login data or not using https'
IP_BLOCK = 'ip address blocked'
INACTIVE_ACCOUNT = 'inactive account'
