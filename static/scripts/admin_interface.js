SIZE_LONG = 30;
SIZE_MEDIUM = 10;
SIZE_SHORT = 3;
SIZE_MINI = 1;

KEY_ENTER = 13;
IGNORED_KEYS = [0, 99, 97];

COLOR_UNSAVED = 'orange';
COLOR_SUCCESS = '#5f5';
COLOR_ERROR = '#FC7682';
COLOR_INPUT = '#eee';

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function show_speedcams_table() {
    $.post("/speedcams_interface/", {'get': 'all'})
        .fail(function(){
            _server_connection_error();
        })
        .success(function(data){
            if(data['success']) {
                _create_speedcams_table(data);
                $('#speedcams_table_set').fadeOut('fast');
                $('#speedcams_table_set').fadeIn('fast');
            }
            else
                alert('Couldn\'t receive data from server.');
        });
}

function _server_connection_error() {
    alert('Failed connecting to server.');
}

function _create_speedcams_table(data) {
    var speedcams = data['data'];
    document.getElementById('ajax_speedcams_table')
        .innerHTML = _draw_table_structure(speedcams);
}

function _draw_table_structure(speedcams) {
    var html = _draw_table_headers();
    html += _draw_table_rows(speedcams);
    return html;
}

function _draw_table_headers() {
    var html = '<tr>';
    html += '<th>ulica</th>';
    html += '<th>godziny</th>';
    html += '<th>dzień</th>';
    html += '<th>miesiąc</th>';
    html += '<th>rok</th>';
    html += '<th>promień</th>';
    html += '<th>szerokość</th>';
    html += '<th>długość</th>';
    html += '<th>usuń</th>';
    html += '</tr>';
    return html;
}

function _draw_table_rows(speedcams) {
    var sorted_ids = _sort_speedcam_ids(speedcams);
    var html = ''
    for(id of sorted_ids) {
        html += '<tr>';
        html += _draw_cells(_get_speedcam_by_id(speedcams, id));
        html += '</tr>';
    } 
    return html;
}

function _sort_speedcam_ids(speedcams) {
    var sorted_ids = []
    for(speedcam of speedcams)
        sorted_ids.push(speedcam['id']);
    return sorted_ids.reverse();
}

function _draw_cells(speedcam) {
    var html = _draw_cell(speedcam, 'street', SIZE_LONG);
    html += _draw_cell(speedcam, 'hours', SIZE_MEDIUM);
    html += _draw_cell(speedcam, 'day', SIZE_MINI);
    html += _draw_cell(speedcam, 'month', SIZE_MINI);
    html += _draw_cell(speedcam, 'year', SIZE_SHORT);
    html += _draw_cell(speedcam, 'radius', SIZE_SHORT);
    html += _draw_cell(speedcam, 'latitude', SIZE_MEDIUM);
    html += _draw_cell(speedcam, 'longitude', SIZE_MEDIUM);
    html += _draw_cell_delete_button(speedcam['id']);
    return html;
}

function _get_speedcam_by_id(speedcams, id) {
   for(speedcam of speedcams)
       if(speedcam['id'] == id)
           return speedcam;
}

function _draw_cell(speedcam, attribute, size) {
    var html = '<td>';
    html += '<input class=table_input';
    html += ' id=input_' + speedcam['id'] + '_' + attribute;
    html += ' size=' + size;
    html += ' onkeypress="collect_data_from_table(event.which, $(this))"';
    html += _draw_value_of_cell(speedcam, attribute);
    html += '</td>';
    return html;
}

function _draw_value_of_cell(speedcam, attribute) {
    if(attribute == 'day' || attribute == 'month' || attribute == 'year')
        if(speedcam[attribute] == -1)
            return ' value="*">';
    return ' value="' + speedcam[attribute] + '">';
}

function _draw_cell_delete_button(id) {
    var html = '<td class=delete_row_button ';
    html += ' onclick="delete_record($(this), ' + id + ')">';
    html += '<center><font color=red size=5 >✗</font></center>';
    html += '</td>';
    return html;
}

function discard_table() {
    $('#speedcams_table_set').fadeOut('fast');
}

function delete_record(handle, speedcam_id) {
    if(!speedcam_id) { // empty row
        _delete_table_row(handle);
        return;
    }
    if(_ask_to_delete(speedcam_id)) {
        _delete_table_row(handle);
        _delete_record_from_database(speedcam_id);
    }
}

function _ask_to_delete(id) {
    var question = 'Usunąć rekord ,,';
    question += $("#input_" + id + "_street").val();
    question += '"?';
    if(confirm(question))
        return true;
    else
        return false;
}

function _delete_table_row(handle) {
    handle.closest('tr').fadeOut('fast', function() {
        handle.closest('tr').remove() });
}

function _delete_record_from_database(id) {
    $.post( "/speedcams_interface/", {'delete': id})
        .fail(function(){
            _server_connection_error();
        })
        .success(function(data){
            if(data['success'])
                console.log('record removed from database');
            else
                alert('error deleting record from database');
        });
}

function collect_data_from_table(key, handle) {
    if(_typing_some_text(key))
        handle.css('background-color', COLOR_UNSAVED);
    if(key == KEY_ENTER)
        _get_data_from_input(handle);
}

function _typing_some_text(key) {
    if(IGNORED_KEYS.indexOf(key) == -1)
       return true;
    else
       return false;
}

// new record dialog box
$(function() {
    $("#new_record_form").dialog({
            autoOpen: false,
            draggable: true,
            open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); },
        });
});

$(function () {
    $("#new_record_form").draggable();
});

function close_new_record_form() {
    $('#new_record_form').dialog('close');
}

function confirm_new_record_form() {
    var new_speedcam = {}
    $('.new_record_input').each(function(){
        var key = $(this).attr('id').split('_')[1];
        var value = $(this).val();
        new_speedcam[key] = value;
    });

    _save_new_speedcam_in_database(new_speedcam);
}

function _save_new_speedcam_in_database(speedcam) {
    $.post("/speedcams_interface/", {'create': JSON.stringify(speedcam)})
        .fail(function(){
            _server_connection_error();
        })
        .success(function(data){
            if(data['success']) {
                _append_table_and_close_form(speedcam, data);
            }
            else if(data['matches']){
                _mark_attributes(data['matches']);
            }
            else {
                alert('Can\'t create new record:\n' + data['error']);
            }
        });

}

function _append_table_and_close_form(speedcam, data) {
    speedcam['id'] = data['id'];
    _append_table(speedcam);
    close_new_record_form();
    _clean_new_record_input_fields();
}

function _clean_new_record_input_fields() {
    $('.new_record_input').css('background-color', COLOR_INPUT);
    $('.new_record_input').val('');
}

function _mark_attributes(matches) {
    var incorrect = [];
    var correct = [];
    _sort_attributes(incorrect, correct, matches);
    _color_input_fields(incorrect, correct);
}

function _sort_attributes(incorrect, correct, matches) {
    for(key of Object.keys(matches)) {
        if(matches[key] == false)
            incorrect.push(key);
        else
            correct.push(key);
    }
}

function _color_input_fields(incorrect, correct) {
    for(item of incorrect)
        $('#form_' + item).css('background-color', COLOR_ERROR);
    for(item of correct)
        $('#form_' + item).css('background-color', COLOR_SUCCESS);
}

function open_new_record_form() {
    $("#new_record_form").dialog('open');
}

function _append_table(speedcam) {
    $('#ajax_speedcams_table tr:first')
        .after('<tr>' + _draw_cells(speedcam) + '</tr>');
}

function _get_data_from_input(handle) {
    var id_attr = handle.attr('id'); // example: input_1_street
    var attribute = id_attr.split('_')[2];
    var id = id_attr.split('_')[1];
    var new_value = handle.val();
    _save_changes_in_database(attribute, id, new_value, handle);
}

function _save_changes_in_database(attribute, id, new_value, handle) {
    $.post("/speedcams_interface/",
           {'edit': id,
            'attribute': attribute,
            'new_value': new_value})
        .fail(function(){
            _server_connection_error();
        })
        .success(function(data){
            if(data['success']) {
                handle.css('background-color', COLOR_SUCCESS);
                console.log('record saved in database');
            }
            else
                handle.css('background-color', COLOR_ERROR);
                // alert('Can\'t edit record:\n' + data['error']);
        });
}
