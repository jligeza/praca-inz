from django.conf.urls import patterns, include, url
from django.contrib import admin
import app.views as views
import app.ajax as ajax
from django.views.generic.base import RedirectView


urlpatterns = patterns(
    '',
    url(r'^$', views.home),
    url(r'^mobile_hello/?$', views.mobile_hello),
    url(r'^mobile_logout/?$', views.mobile_logout),
    url(r'^mobile_login/?$', views.mobile_login),
    url(r'^mobile_register/?$', views.mobile_register),
    url(r'^mobile_password_reset/?$', views.mobile_password_reset),
    url(r'^speedcams_interface/$', ajax.speedcams_interface),
    url(r'^admin_interface/$', views.admin_interface),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/password/change/done/$', views.password_change_wrapper),
    url(r'^accounts/password/reset/done/$', views.password_reset_wrapper),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^accounts/profile/', RedirectView.as_view(url="/")),
)
