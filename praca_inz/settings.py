#!/usr/bin/env python
# -*- coding: utf-8 -*-
import secrets

"""
Django settings for praca_inz project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = secrets.SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = TEMPLATE_DEBUG = secrets.DEBUG

ALLOWED_HOSTS = ['127.0.0.1', 'praca-inz.vipserv.org', 'www.praca-inz.vipserv.org']

ADMINS = (
    ('Jakub Ligęza', 'dagins92@gmail.com'),
)

SERVER_EMAIL = secrets.EMAIL_HOST_USER


# Application definition

INSTALLED_APPS = (
    'django.core.mail',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'registration',
    'app',
)

MIDDLEWARE_CLASSES = (
    secrets.sslify,
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

SITE_ID = 1

# dni dane na aktywację maila i autologowanie
# po kliknięciu linka aktywacyjnego
ACCOUNT_ACTIVATION_DAYS = 5
REGISTRATION_AUTO_LOGIN = True

EMAIL_HOST = secrets.EMAIL_HOST
EMAIL_PORT = secrets.EMAIL_PORT
EMAIL_HOST_USER = secrets.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = secrets.EMAIL_HOST_PASSWORD
EMAIL_USE_SSL = secrets.EMAIL_USE_SSL

"""
    Gmail SMTP port (TLS): 587
    Gmail SMTP port (SSL): 465
    Gmail SMTP TLS/SSL required: yes
"""

ROOT_URLCONF = 'praca_inz.urls'

WSGI_APPLICATION = 'praca_inz.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = secrets.DATABASES

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pl'

TIME_ZONE = 'Europe/Warsaw'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'


STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': './logs.log',
            'formatter': 'custom',
        },
    },
    'formatters': {
        'custom': {
            'format': '[%(asctime)s] <%(levelname)s>: %(message)s',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'authentication': {
            'handlers': ['file'],
            'level': 'INFO',
        },
        'registration': {
            'handlers': ['file'],
            'level': 'INFO',
        },
    },
}

from app import signals
# logging account registration and activation
signals.connect_signals()
